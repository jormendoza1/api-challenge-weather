import { Controller, Get, Param, UseGuards } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { ApiKeyGuard } from '../auth/guards/api-key.guard'
import { ForecastsService } from './forecasts.service'

@ApiTags('Forecasts')
@UseGuards(ApiKeyGuard)
@Controller('forecast')
export class ForecastsController {
  constructor(private forecastsService: ForecastsService) {}

  @ApiOperation({
    summary: 'Obtiene datos ubicacion city desde ip-api, segun la IP que recibe como parametro.  Tambien obtiene el estado del tiempo a 5 días para esa localización ',
  })
  @Get(':ip')
  getDataCityForecast(@Param('ip') ip: string) {
    return this.forecastsService.findDataCityForecast(ip);
  }
  @ApiOperation({
    summary: 'Obtiene datos de localizacion de la city segun un parametro de busqueda. Tambien obtiene el estado del tiempo a 5 días para esa localización',
  })
  @Get('/city/:city')
  getDataCitySearchWeather(@Param('city') city: string) {
    return this.forecastsService.findDataCitySearchWeather(city)
  }
}
