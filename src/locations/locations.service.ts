import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class LocationsService {
  constructor(private httpService: HttpService, private configService: ConfigService) {}

  async findDataCity(ip: string): Promise<any> {
    try {
      const urlBase = this.configService.get('URL_BASE_IP_API')
      const endpoint = '/json/' + ip
      const url = urlBase + endpoint
      const response = await this.httpService.get(url).toPromise()
      return response.data
    } catch (err) {
      return {
        message: err.message,
      }
    }
  }

  async findDataSearchCity(city: string): Promise<any> {
    try {
      const urlBase = this.configService.get('URL_BASE_ACUWEATHER')
      const endpoint = '/locations/v1/search?apikey=' + this.configService.get('API_KEY_ACUWEATHER') + '&q=' + city
      const url = urlBase + endpoint
      const response = await this.httpService.get(url).toPromise()
      if (response.data.length < 1) {
        throw new HttpException({
          status: HttpStatus.FORBIDDEN,
          error: 'City no encontrada....',
          },
          HttpStatus.FORBIDDEN,
        )
      }
      return response.data
    } catch (err) {
      return {
        error: err.status,
        message: err.response.error,
      }
    }
  }
}
