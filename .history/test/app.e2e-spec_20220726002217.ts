import { Test, TestingModule } from '@nestjs/testing'
import * as request from 'supertest'
import { AppModule } from '../src/app.module'
import { INestApplication, HttpService, HttpModule } from '@nestjs/common'
import { AppService } from '../src/app.service'
import { LocationsService } from '../src/locations/locations.service'
import { CurrentsService } from '../src/currents/currents.service'
import { ForecastsService } from '../src/forecasts/forecasts.service'
import { LocationsModule } from '../src/locations/locations.module'
import { CurrentsModule } from '../src/currents/currents.module'
import { ForecastsModule } from '../src/forecasts/forecasts.module'

describe('Endpoints (e2e)', () => {
  let app: INestApplication
  let httpService: HttpService

  beforeAll(async () => {
    const mockAppModule: TestingModule = await Test.createTestingModule({
      imports: [AppModule, HttpModule, LocationsModule, CurrentsModule, ForecastsModule],
      providers: [AppService, LocationsService, CurrentsService, ForecastsService],
    }).compile()

    app = mockAppModule.createNestApplication()
    httpService = mockAppModule.get<HttpService>(HttpService)
    await app.init()
  })

  describe('Test Iniciales para determinar la ruta', () => {
    it('GET Hello from /', async () => {
      const response = await request(app.getHttpServer()).get('/').expect(200)
      expect(response.text).toEqual('Hello World!')
    })
  })

  describe('Test de Endpoints Weather', () => {
    const ipAddress = '181.177.14.136';
    const authKey = 'test-challenge';

    it('Devuelve los datos de ubicación (lat y lon) city según ip-api: GET /location/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/location/' + ipAddress)
        .set('Auth', authKey)
        .timeout(20000)
        .expect(200)
      expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
      expect(response.body.lat).toBeDefined()
      expect(response.body.lon).toBeDefined()
      // console.log(response)
    })

    it('Devuelve los datos de ubicación city o la ubicación actual según ip-api y el estado del tiempo actual. : GET /current/:ip', async () => {
         const response = await request(app.getHttpServer())
        .get('/current/' + ipAddress)
        .set('Auth', authKey)
        .timeout(20000)
        .expect(200)
      expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
      expect(response.body.ipapiDataCity.lat).toBeDefined()
      expect(response.body.ipapiDataCity.lon).toBeDefined()
      expect(response.body.ipapiDataCity.country).toBeDefined()
      expect(response.body.ipapiDataCity.regionName).toBeDefined()
      expect(response.body.ipapiDataCity.city).toBeDefined()

      expect(response.body.accuweatherCurrent[0].WeatherText).toBeDefined()
      expect(response.body.accuweatherCurrent[0].Temperature).toBeDefined()

      // console.log('CURRENT', response)
    })

    it('Devuelve los datos de ubicación city o la ubicación actual según ip-api y el estado del tiempo a 5 días  : GET /forecast/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/forecast/' + ipAddress)
        .set('Auth', authKey)
        .timeout(20000)
        .expect(200)
      expect(response.headers['content-type']).toEqual(expect.stringContaining('json'))
      expect(response.body.ipapiDataCity.lat).toBeDefined()
      expect(response.body.ipapiDataCity.lon).toBeDefined()
      expect(response.body.ipapiDataCity.country).toBeDefined()
      expect(response.body.ipapiDataCity.regionName).toBeDefined()
      expect(response.body.ipapiDataCity.city).toBeDefined()

      expect(response.body.accuweatherForecast.Headline).toBeDefined()
      expect(response.body.accuweatherForecast.DailyForecasts).toBeDefined()
      // console.log('FORECAST: ', response)
    })
  })

  describe('Test de Error - Endpoints Weather', () => {
    const ipAddress = '181.177.14.136';
    const authKey = 'test-challenge';

    it('Devuelve error si no envia ip: GET /location/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/location/' + '')
        .set('Auth', authKey)
        .expect(404)
    })
    it('Devuelve error si no manda el Auth Key: GET /location/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/location/' + ipAddress)
        .set('Auth', '')
        .expect(401)
      expect(response.body.message).toEqual('No tienes permitido el acceso - api challenge weather')
    })

    it('Devuelve error si no envia ip. : GET /current/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/current/' + '')
        .set('Auth', authKey)
        .expect(404)
    })
    it('Devuelve error si no envia el Auth Key: GET /current/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/current/' + ipAddress)
        .set('Auth', '')
        .expect(401)
      expect(response.body.message).toEqual('No tienes permitido el acceso - api challenge weather')
    })

    it('Devuelve error si no envia ip  : GET /forecast/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/forecast/' + '')
        .set('Auth', authKey)
        .expect(404)
    })
    it('Devuelve error si no envia el Auth Key: GET /forecast/:ip', async () => {
      const response = await request(app.getHttpServer())
        .get('/forecast/' + ipAddress)
        .set('Auth', '')
        .expect(401)
      expect(response.body.message).toEqual('No tienes permitido el acceso - api challenge weather')
    })
  })
})
