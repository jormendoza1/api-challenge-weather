import { HttpService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { LocationsService } from './locations.service';

describe('LocationsService', () => {
  let service: LocationsService;
  

  const mockLocationsRepository = () => ({
    findDataCity: jest.fn(),
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpService, ConfigService],
      providers: [LocationsService],
    }).compile();

    service = module.get<LocationsService>(LocationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findDataCity', () => {
    it('should get data city by ip address', async () => {
      const result = await service.findDataCity('181.177.14.136');
      expect(result).toEqual('someDataCity');
    });
  });

});
